package cn.xsshome.mvcdo.util;
/**
 * 微信相关常量值
 * @author 小帅丶
 */
public class WeChatConstant {
	/**
	 * 微信小程序的APPID
	 */
	public static final String WCSP_APPID="wxd992e5bb18f92db8";
	/**
	 * 微信小程序的APPSECRET
	 */
	public static final String WCSP_APPSECRET="1551d017d0291144e48e16b7b65bba25";
	/**
	 * 获取session_key秘钥接口地址
	 */
	public static final String JSCODE2SESSION_URL="https://api.weixin.qq.com/sns/jscode2session";
	/**
	 * 授权类型
	 */
	public static final String GRANT_TYPE="authorization_code";
}
